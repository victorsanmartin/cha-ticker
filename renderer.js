const url = 'https://api.orionx.io/graphql';
const https = require('https');

const numberWithDots = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

function getQuery(coinType) {
    return `{
      market(code: "${coinType}") {
        lastTrade {
          price
        }
      }
      marketCurrentStats(marketCode: "${coinType}", aggregation: d1) {
  		  open
  		  close
  		  high
  		  low
  		  variation
          volume
      }
    }`;
}

function getOrionXPrice(coinType) {
    return new Promise((resolve, reject) => {
        let body = '';
        let query = getQuery(coinType);
        https
            .get(`${url}?query=${query}`, resp => {
                resp.on('data', chunk => {
                    body += chunk;
                });
                resp.on('end', () => {
                    try {
                        const json = JSON.parse(body);
                        if (json.data) {
                            resolve({
                                price: json.data.market.lastTrade.price,
                                open: json.data.marketCurrentStats.open,
                                close: json.data.marketCurrentStats.close,
                                high: json.data.marketCurrentStats.high,
                                low: json.data.marketCurrentStats.low,
                                variation: json.data.marketCurrentStats.variation,
                                volume: json.data.marketCurrentStats.volume
                            });
                        } else {
                            reject();
                        }
                    } catch (e) {
                        reject();
                    }
                });
            })
            .on('error', err => {
                console.log('Error: ' + err.message);
            });
    });
}

const coins = ['CHACLP'];
const types = ['price', 'open', 'close', 'high', 'low'];
function check() {
    coins.forEach(coin => {
        getOrionXPrice(coin)
            .then(result => {
                types.forEach(type => {
                    document.getElementById(coin + '-' + type).innerHTML = numberWithDots(result[type]);
                });
                document.getElementById(coin + '-variation').innerHTML = (result.variation * 100).toFixed(2) + '%';
                document.getElementById(coin + '-volume').innerHTML = (result.volume / 100000000).toFixed(2);
            })
            .catch(err => {
                console.log(err);
            });
    });
}

check(); // Check at startup ;)
setInterval(function() {
    check();
}, 2 * 60 * 1000); // Check every 15 secs
